import { Routes } from '@angular/router';

export const routes: Routes = [
    {
    path: '',
    loadChildren: () =>
      import('./crypto/crypto.module').then((m) => m.CryptoModule),
  },
];
