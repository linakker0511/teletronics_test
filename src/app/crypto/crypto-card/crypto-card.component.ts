import { Component, OnInit } from '@angular/core';
import { CryptoModel } from '../models/crypto.model';
import { CryptoService } from '../services/crypto.service';

@Component({
  selector: 'app-crypto-card',
  templateUrl: './crypto-card.component.html',
  styleUrl: './crypto-card.component.css',
})
export class CryptoCardComponent implements OnInit {
  cryptoList: CryptoModel[] = [];

  constructor(private cryptoService: CryptoService) {}

  ngOnInit(): void {
    this.getAllCrypto();
  }

  getAllCrypto() {
    this.cryptoService.getCrypto().subscribe({
      next: (result) => {
        this.cryptoList = result;
      },
      error: (error) => {
        console.error('Error fetching data: ', error);
      },
    });
  }

  removeCrypto(id: string) {
    this.cryptoService.removeCrypto(id);
  }
}
