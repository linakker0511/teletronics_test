import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CryptoCardComponent } from './crypto-card.component';
import { CryptoService } from '../services/crypto.service';
import { of } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../shared/material/material.module';
import { ButtonComponent } from '../../../stories/button.component';
import { CryptoHeaderComponent } from '../../../stories/crypto-header.component';

const cryptoServiceMock = {
  getCrypto: jasmine.createSpy('getCrypto').and.returnValue(
    of([
      { id: '1', name: 'Bitcoin', priceUsd: '50000' },
      { id: '2', name: 'Ethereum', priceUsd: '4000' },
    ]),
  ),
  removeCrypto: jasmine.createSpy('removeCrypto'),
};

describe('CryptoCardComponent', () => {
  let component: CryptoCardComponent;
  let fixture: ComponentFixture<CryptoCardComponent>;
  let service: CryptoService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        MatButtonModule,
        NoopAnimationsModule,
        MaterialModule,
        ButtonComponent,
        CryptoHeaderComponent,
      ],
      declarations: [CryptoCardComponent],
      providers: [{ provide: CryptoService, useValue: cryptoServiceMock }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoCardComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(CryptoService);
    fixture.detectChanges(); 
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getCrypto on ngOnInit', () => {
    expect(service.getCrypto).toHaveBeenCalled();
    expect(component.cryptoList.length).toBe(2);
  });

  it('should display crypto cards for each item in the list', () => {
    const cardElements = fixture.nativeElement.querySelectorAll('.card-style');
    expect(cardElements.length).toBe(2); 
    expect(cardElements[0].textContent).toContain('Bitcoin');
    expect(cardElements[1].textContent).toContain('Ethereum');
  });

  it('should call removeCrypto on the service when a card remove button is clicked', () => {
    const firstCardButton = fixture.nativeElement.querySelector(
      '.card-action button',
    );
    firstCardButton.click();
    expect(service.removeCrypto).toHaveBeenCalledWith('1'); 
  });
});
