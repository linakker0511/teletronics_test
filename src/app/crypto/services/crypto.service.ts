import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CryptoModel } from '../models/crypto.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CryptoService {
  
  private assetsSource = new BehaviorSubject<CryptoModel[]>([]);
  currentAssets = this.assetsSource.asObservable();

  constructor(private http: HttpClient) {}

  getCrypto(): Observable<CryptoModel[]> {
    this.http
      .get<any>(`http://api.coincap.io/v2/assets`)
      .pipe(map((response) => response.data))
      .subscribe((data) => {
        this.assetsSource.next(data);
      });

    return this.currentAssets;
  }

  addCrypto(asset: CryptoModel) {
    const currentValue = this.assetsSource.value;
    this.assetsSource.next([asset,...currentValue]);
  }

  removeCrypto(id: string) {
    const updatedAssets = this.assetsSource.value.filter(
      (asset) => asset.id !== id,
    );
    console.log('updatedAssets', updatedAssets);
    this.assetsSource.next(updatedAssets);
  }
}
