import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CryptoDashboardComponent } from './crypto-dashboard/crypto-dashboard.component';
import { CryptoCardComponent } from './crypto-card/crypto-card.component';
import { CryptoAddComponent } from './crypto-add/crypto-add.component';
import { MaterialModule } from '../shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from '../../stories/button.component';
import { CryptoHeaderComponent } from '../../stories/crypto-header.component';

@NgModule({
  declarations: [
    CryptoDashboardComponent,
    CryptoCardComponent,
    CryptoAddComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonComponent,
    CryptoHeaderComponent,
  ],
  exports: [
    MaterialModule,
    CryptoDashboardComponent,
    CryptoCardComponent,
    CryptoAddComponent,
  ],
})
export class CryptoModule {}
