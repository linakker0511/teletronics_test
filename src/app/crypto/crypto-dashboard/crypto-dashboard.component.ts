import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CryptoAddComponent } from '../crypto-add/crypto-add.component';
import { CryptoService } from '../services/crypto.service';

@Component({
  selector: 'app-crypto-dashboard',
  templateUrl: './crypto-dashboard.component.html',
  styleUrl: './crypto-dashboard.component.css',
})
export class CryptoDashboardComponent implements OnInit {

  constructor(
    private cryptoService: CryptoService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {}

  openAddCryptoModal() {
    const dialogRef = this.dialog.open(CryptoAddComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.cryptoService.addCrypto(result);
      }
    });
  }
}
