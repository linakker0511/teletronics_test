import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { CryptoDashboardComponent } from './crypto-dashboard.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from '../../shared/material/material.module';
import { ButtonComponent } from '../../../stories/button.component';
import { CryptoHeaderComponent } from '../../../stories/crypto-header.component';
import { CryptoService } from '../services/crypto.service';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CryptoDashboardComponent', () => {
  let component: CryptoDashboardComponent;
  let fixture: ComponentFixture<CryptoDashboardComponent>;
  let mockDialog: jasmine.SpyObj<MatDialog>;

  const cryptoServiceMock = {
    getCryptos: () => of([]), 
    addCrypto: jasmine.createSpy('addCrypto')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CryptoDashboardComponent],
      imports: [
        MatDialogModule,
        NoopAnimationsModule,
        MaterialModule,
        ButtonComponent,
        CryptoHeaderComponent,
        HttpClientTestingModule
      ],
      providers: [
        { provide: MatDialog, useValue: mockDialog },
        { provide: CryptoService, useValue: cryptoServiceMock } 
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  });
  

  beforeEach(async () => {
    
    mockDialog = jasmine.createSpyObj('MatDialog', ['open']);

    await TestBed.configureTestingModule({
      declarations: [CryptoDashboardComponent],
      providers: [
        { provide: MatDialog, useValue: mockDialog }
      ],
      imports: [
        NoopAnimationsModule, 
        MatDialogModule,

      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA] 
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); 
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
