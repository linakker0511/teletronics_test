import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CryptoService } from '../services/crypto.service';
import { CryptoAddComponent } from './crypto-add.component';
import { of } from 'rxjs';
import { ButtonComponent } from '../../../stories/button.component';
import { CryptoHeaderComponent } from '../../../stories/crypto-header.component';
import { MaterialModule } from '../../shared/material/material.module';

describe('CryptoAddComponent', () => {
  let component: CryptoAddComponent;
  let fixture: ComponentFixture<CryptoAddComponent>;
  let cryptoServiceMock: any;
  let dialogRefMock: any;

  beforeEach(async () => {
    cryptoServiceMock = jasmine.createSpyObj('CryptoService', ['addCrypto']);
    dialogRefMock = jasmine.createSpyObj('MatDialogRef', ['close']);

    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        NoopAnimationsModule,
        MaterialModule,
        ButtonComponent,
        CryptoHeaderComponent,
      ],
      declarations: [CryptoAddComponent],
      providers: [
        { provide: CryptoService, useValue: cryptoServiceMock },
        { provide: MatDialogRef, useValue: dialogRefMock },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CryptoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.addCryptoForm.valid).toBeFalsy();
  });

  it('form should be valid with required fields filled', () => {
    component.addCryptoForm.controls['id'].setValue('1');
    component.addCryptoForm.controls['rank'].setValue('1');
    component.addCryptoForm.controls['symbol'].setValue('BTC');
    component.addCryptoForm.controls['name'].setValue('BTC');
    component.addCryptoForm.controls['supply'].setValue('1000');
    component.addCryptoForm.controls['maxSupply'].setValue('99999');
    component.addCryptoForm.controls['marketCapUsd'].setValue('78787.00');
    component.addCryptoForm.controls['volumeUsd24Hr'].setValue('7898789.090');
    component.addCryptoForm.controls['priceUsd'].setValue('10000');
    component.addCryptoForm.controls['changePercent24Hr'].setValue('20');
    component.addCryptoForm.controls['vwap24Hr'].setValue('12');
    expect(component.addCryptoForm.valid).toBeTruthy();
  });

  it('should call addCrypto on the service when addCrypto is called and form is valid', () => {
    spyOn(component, 'addCrypto').and.callThrough();
    component.addCryptoForm.controls['id'].setValue('1');
    component.addCryptoForm.controls['rank'].setValue('1');
    component.addCryptoForm.controls['symbol'].setValue('BTC');
    component.addCryptoForm.controls['name'].setValue('BTC');
    component.addCryptoForm.controls['supply'].setValue('1000');
    component.addCryptoForm.controls['maxSupply'].setValue('99999');
    component.addCryptoForm.controls['marketCapUsd'].setValue('78787.00');
    component.addCryptoForm.controls['volumeUsd24Hr'].setValue('7898789.090');
    component.addCryptoForm.controls['priceUsd'].setValue('10000');
    component.addCryptoForm.controls['changePercent24Hr'].setValue('20');
    component.addCryptoForm.controls['vwap24Hr'].setValue('12');
    component.addCrypto();
    expect(cryptoServiceMock.addCrypto).toHaveBeenCalled();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });

  it('should reset form and close dialog after adding a crypto asset', () => {
    component.addCryptoForm.controls['id'].setValue('1');
    component.addCryptoForm.controls['rank'].setValue('1');
    component.addCryptoForm.controls['symbol'].setValue('BTC');
    component.addCryptoForm.controls['name'].setValue('BTC');
    component.addCryptoForm.controls['supply'].setValue('1000');
    component.addCryptoForm.controls['maxSupply'].setValue('99999');
    component.addCryptoForm.controls['marketCapUsd'].setValue('78787.00');
    component.addCryptoForm.controls['volumeUsd24Hr'].setValue('7898789.090');
    component.addCryptoForm.controls['priceUsd'].setValue('10000');
    component.addCryptoForm.controls['changePercent24Hr'].setValue('20');
    component.addCryptoForm.controls['vwap24Hr'].setValue('12');
    component.addCrypto();
    expect(component.addCryptoForm.pristine).toBeTruthy();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });

  it('should close dialog when close is called', () => {
    component.close();
    expect(dialogRefMock.close).toHaveBeenCalled();
  });
});
