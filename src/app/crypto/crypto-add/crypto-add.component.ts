import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CryptoService } from '../services/crypto.service';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
@Component({
  selector: 'app-crypto-add',
  templateUrl: './crypto-add.component.html',
  styleUrl: './crypto-add.component.css',
})
export class CryptoAddComponent {
  addCryptoForm!: FormGroup;
  formFields = [
    { label: 'ID', name: 'id', type: 'text' },
    { label: 'Rank', name: 'rank', type: 'number' },
    { label: 'Symbol', name: 'symbol', type: 'text' },
    { label: 'Name', name: 'name', type: 'text' },
    { label: 'Price USD', name: 'priceUsd', type: 'number' },
    { label: 'Supply', name: 'supply', type: 'number' },
    { label: 'Max Supply', name: 'maxSupply', type: 'number' },
    { label: 'Market Cap USD', name: 'marketCapUsd', type: 'number' },
    { label: 'Volume USD 24Hr', name: 'volumeUsd24Hr', type: 'number' },
    { label: 'Change Percent 24Hr', name: 'changePercent24Hr', type: 'number' },
    { label: 'VWAP 24Hr', name: 'vwap24Hr', type: 'number' },
  ];

  constructor(
    public dialogRef: MatDialogRef<CryptoAddComponent>,
    private fb: FormBuilder,
    private cryptoService: CryptoService,
  ) {
    this.addCryptoForm = this.fb.group({
      id: ['', Validators.required],
      rank: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      symbol: ['', Validators.required],
      name: ['', Validators.required],
      priceUsd: ['', Validators.required],
      supply: [
        '',
        // Validators.required,
      ],
      maxSupply: [
        '',
        // Validators.required,
      ],
      marketCapUsd: [
        '',
        // Validators.required,
      ],
      volumeUsd24Hr: [
        '',
        // Validators.required,
      ],
      changePercent24Hr: [
        '',
        // Validators.required,
      ],
      vwap24Hr: [
        '',
        // Validators.required,
      ],
    });
  }
  addCrypto() {
    if (this.addCryptoForm.valid) {
      this.cryptoService.addCrypto(this.addCryptoForm.value);
      this.dialogRef.close();
      this.addCryptoForm.reset();
    }
  }

  close() {
    this.dialogRef.close();
  }
}
