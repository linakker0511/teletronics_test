import { Component, Input, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonComponent } from './button.component';

@Component({
  selector: 'storybook-crypto-header',
  standalone: true,
  imports: [CommonModule, ButtonComponent],
  template: `<header class="storybook-cryto-header">
    <div class="storybook-crypto-header">
      <div>
        <h1>{{ label }}</h1>
      </div>
      <div>
        <div>
          <storybook-button
            *ngIf="addCrypto"
            size="small"
            class="margin-left"
            (onClick)="onAdd.emit($event)"
            label="+ Add"
          ></storybook-button>
        </div>
      </div>
    </div>
  </header>`,
  styleUrls: ['./crypto-header.css'],
})
export class CryptoHeaderComponent {
  @Input()
  label = 'Live Crypto';

  @Input()
  addCrypto: any | null = null;

  @Output()
  onAdd = new EventEmitter<Event>();
}
