import type { Meta, StoryObj } from '@storybook/angular';

import { fn } from '@storybook/test';
import { CryptoHeaderComponent } from './crypto-header.component';

const meta: Meta<CryptoHeaderComponent> = {
  title: 'Example/CryptoHeader',
  component: CryptoHeaderComponent,
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
    layout: 'fullscreen',
  },
  args: {
    onAdd: fn(),
  },
};

export default meta;
type Story = StoryObj<CryptoHeaderComponent>;

export const Add: Story = {
  args: {},
};
